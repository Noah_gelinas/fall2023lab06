//Noah Gelinas
package backend;

import java.util.Random;

public class RpsGame {
    private int wins=0;
    private int losses=0;
    private int ties=0;

    Random rand = new Random();

    public int getWins() {
        return this.wins;
    }

    public int getLossess() {
        return this.losses;
    }

    public int getTies() {
        return this.ties;
    }

    public String playRound(String choice) {
        int computerChoice = rand.nextInt(3);
        String recentGame="";
        if (choice.equals("rock") && computerChoice==0) {
            this.ties++;
            recentGame="its a tie!";
        }
        else if (choice.equals("rock") && computerChoice==1) {
            this.wins++;
            recentGame="the User Won!";
        }
        else if (choice.equals("rock") && computerChoice==2) {
            this.losses++;
            recentGame="the Computer Won :(";
        }
        else if (choice.equals("paper") && computerChoice==0) {
            this.wins++;
            recentGame="the User Won!";
        }
        else if (choice.equals("paper") && computerChoice==1) {
            this.losses++;
            recentGame="the Computer Won :(";
        }
        else if (choice.equals("paper") && computerChoice==2) {
            this.ties++;
            recentGame="its a tie!";
        }
        else if (choice.equals("scissors") && computerChoice==0) {
            this.losses++;
            recentGame="the Computer Won :(";
        }
        else if (choice.equals("scissors") && computerChoice==1) {
            this.ties++;
            recentGame="its a tie!";
        }
        else if (choice.equals("scissors") && computerChoice==2) {
            this.wins++;
            recentGame="the User Won!";
        }
        String compChoiceString = "";
        if (computerChoice==0) {
            compChoiceString="rock";
        }else if(computerChoice==1) {
            compChoiceString="scissors";
        }else if(computerChoice==2) {
            compChoiceString="paper";
        }
        return ("\nComputer plays " + compChoiceString+" and " + recentGame);
    }
}
