//Noah Gelinas
package rpsgame;
import java.util.Scanner;
import backend.RpsGame;
/**
 * Console application for rock paper scissors game
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        RpsGame newGame = new RpsGame();
        String choice="";
        Scanner scan = new Scanner(System.in);
        boolean exit=false;
        while (exit==false) {
            boolean badInput=true;
            while (badInput) {
                System.out.println("\nEnter your choice! either 'rock' | 'scissors' | 'paper' ");
                choice=scan.nextLine();
                if(choice.equals("rock") || choice.equals("scissors") || choice.equals("paper")) {
                    badInput=false;
                }else{
                    badInput=true;
                }
            }
            System.out.println(newGame.playRound(choice));
            badInput=true;
            while(badInput) {
                System.out.println("\nWould you like to play again? Enter 'Yes' for yes or 'No' for No");
                String answer=scan.nextLine();

                if (answer.equals("Yes") || answer.equals("No")) {
                    badInput=false;
                }else {
                    badInput=true;
                }
                if (answer.equals("Yes")) {
                    exit=false;
                }else{
                    exit=true;
                }
            }
        }
    }
}
